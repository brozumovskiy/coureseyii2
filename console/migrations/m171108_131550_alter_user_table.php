<?php

use yii\db\Migration;

class m171108_131550_alter_user_table extends Migration
{
    public function Up()
    {
        $this->addColumn('{{%user}}', 'about', $this->text());
        $this->addColumn('{{%user}}', 'type', $this->integer(3));
        $this->addColumn('{{%user}}', 'nickname', $this->string(70));
        $this->addColumn('{{%user}}', 'picture', $this->string());
    }

    public function Down()
    {
        echo "m171108_131550_alter_user_table cannot be reverted.\n";

        $this->dropColumn('{{%user}}', 'about');
        $this->dropColumn('{{%user}}', 'nickname');
        $this->dropColumn('{{%user}}', 'picture');
        $this->dropColumn('{{%user}}', 'type');
    }

}   