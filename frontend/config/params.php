<?php
return [
    'adminEmail' => 'admin@example.com',
    
    'maxFileSize' => 1024 * 1024 * 2, // 2 megabites
    'storagePath' => '@frontend/web/uploads/',
    'storageUri' => '/uploads/',  //http://images.com/uploads/75/57/e1b8a0993c0dcadc5b619ca6b88046497f97.jpg
    
    'postPicture' => [
        'maxWidth' => 1024,
        'maxHeight' => 768,
        ],
    
    'profilePicture' => [
        'maxWidth' => 1280,
        'maxHeight' => 1024,
    ],
    
    'feedPostLimit' => 200,
];
