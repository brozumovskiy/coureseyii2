$(document).ready(function () {
    $('.button-delete').click(function () {
        var params = {
            'id': $(this).attr('data-id')
        };

        $(this).parent().remove();

        $.post('/post/default/delete', params, function (data) {
            if (data.success) {
            }
        });

        return false;
    });

    $('#pagecomment').on('click', '.button-edit', function () {

        var params = {
            'id': $(this).attr('data-id'),
            'text': document.getElementById('somecomment').value
        };

        $.post('/post/default/update', params, function (data) {
            if (data.success) {
                
            }
        });

        return false;
    });

});