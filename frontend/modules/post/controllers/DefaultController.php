<?php

namespace frontend\modules\post\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use frontend\models\Post;
use frontend\modules\post\models\forms\PostForm;

/**
 * Default controller for the `post` module
 */
class DefaultController extends Controller {

    /**
     * Renders the create view for the module
     * @return string
     */
    public function actionCreate() {

        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        $model = new PostForm(Yii::$app->user->identity);

        if ($model->load(Yii::$app->request->post())) {

            $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->save()) {

                Yii::$app->session->setFlash('success', 'Post created!');
                return $this->goHome();
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Renders the create view for the module
     * @return string
     */
    public function actionView($id) {
        /* @var $currentUser User */

        $currentUser = Yii::$app->user->identity;

        $post = $this->findPost($id);

        $model = new \frontend\modules\post\models\forms\CommentForm(Yii::$app->user->identity, Post::findOne($id));

        $commentList = \frontend\models\Comment::find()->where(['post_id' => $id])->orderBy('id')->all();


        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {

                Yii::$app->session->setFlash('success', 'Coment added!');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('view', [
                    'post' => $this->findPost($id),
                    'currentUser' => $currentUser,
                    'model' => $model,
                    'commentList' => $commentList,
        ]);
    }

    /**
     * 
     *      * Updates an existing Book model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate() {
        Yii::$app->response->format = Response::FORMAT_JSON;


        $id = Yii::$app->request->post('id');
        $text = Yii::$app->request->post('text');

        $model = $this->findComment($id);
        $model->text = $text;



        if ($model->save()) {

            Yii::$app->session->setFlash('success', 'Coment updated!');
        }
    }

    public function actionLike() {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');
        $post = $this->findPost($id);

        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;

        $post->like($currentUser);

        return [
            'success' => true,
            'likesCount' => $post->countLikes(),
        ];
    }

    public function actionUnlike() {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');

        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;
        $post = $this->findPost($id);

        $post->unLike($currentUser);

        return [
            'success' => true,
            'likesCount' => $post->countLikes(),
        ];
    }

    /**
     * Deletes an existing Comment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete() {

        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');
        $comment = $this->findComment($id);
        $comment->delete();

        return [
            'success' => true,
        ];
    }

    private function findPost($id) {
        if ($user = Post::findOne($id)) {
            return $user;
        }
        throw new \yii\web\NotFoundHttpException;
    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findComment($id) {
        if (($model = \frontend\models\Comment::findOne($id))) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findText($text) {
        if (($model = \frontend\models\Comment::findOne($text))) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
