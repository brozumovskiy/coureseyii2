<?php
/* @var $this yii\web\View */
/* @var $post frontend\models\Post */
/* @var $comment frontend\models\Comment */
/* @var $commentList[] frontend\models\Comment */

use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\bootstrap\ActiveForm;
?>

<div class="post-default-index">

    <div class="row">

        <div class="col-md-12">
            <?php if ($post->user): ?>
            <?php echo $post->user->username; ?>
            <?php endif; ?>
        </div>

        <div class="col-md-12">
            <b>
                <?php
                const PAGE_DATE_FORMAT = 'Y-m-d H:i:s';
                echo 'Date of publication post: ';
                echo date(PAGE_DATE_FORMAT, $post->create_at)
                ?>
            </b>
        </div>

        <div class="col-md-12">
            <img src="<?php echo $post->getImage(); ?>" />
        </div>

        <div class="col-md-12">
            <?php echo Html::encode($post->description); ?>
        </div>

    </div>

    <hr>

    <div class="col-md-12">
        Likes: <span class="likes-count"><?php echo $post->countLikes(); ?></span>

        <a href="#" class="btn btn-primary button-unlike <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "" : "display-none"; ?>" data-id="<?php echo $post->id; ?>">
            Unlike&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-down"></span>
        </a>
        <a href="#" class="btn btn-primary button-like <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "display-none" : ""; ?>" data-id="<?php echo $post->id; ?>">
            Like&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-up"></span>
        </a>

    </div>

</div>



<?php
$this->registerJsFile('@web/js/likes.js', [
'depends' => JqueryAsset::className(),
]);
?>

<?php
$this->registerJsFile('@web/js/comment.js', [
'depends' => JqueryAsset::className(),
]);
?>
<hr>

<div class="post-default-index">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($model, 'text')->textInput()->label('Add your comment please')->textarea(['rows' => '2', 'cols' => '5']);

    echo Html::submitButton('Add comment', [
    "class" => 'btn btn-primary button-create',
    ]);
    ?>

    <?php ActiveForm::end(); ?>

</div>

<hr>

<?php foreach ($commentList as $comment): ?>
<div class="col-md-10">

    <?php echo "User id: " . $comment->user_id; ?>
    <strong>
        (<?php
        echo 'Date of publication comment: ';
        echo date(PAGE_DATE_FORMAT, $comment->create_at);
        ?>)</strong>
    <p> 
  
        
        
        <?php if ($currentUser->id === $comment->user_id): ?>
        
    <div id="pagecomment">
        
        <textarea id="somecomment" rows="5" cols="35" ><?php echo $comment->text; ?></textarea><br>
        
        <a href="#" class="btn btn-primary button-edit"  data-id="<?php echo $comment->id; ?>" >
            Edit
        </a>
        <a href="#" class="btn btn-danger button-delete"  data-id="<?php echo $comment->id; ?>">
            Delete
        </a>
  
    </div>

    <?php else: ?>
    <div>
        <?php echo $comment->text; ?>
    </div>

    <?php endif; ?>
    <hr>
        
</div>
<?php endforeach; ?>

     






