<?php

namespace frontend\modules\post\models\forms;

use Yii;
use yii\base\Model;
use frontend\models\Comment;
use frontend\models\User;
use frontend\models\Post;

class CommentForm extends Model {

    const MAX_DESCRIPTIOM_LENGHT = 1000;

    public $text;
    
    private $user;
    private $post;

    /**
     * 
     * @inheritdoc
     */
    public function rules() {
        return [
            [['text'], 'string', 'max' => self::MAX_DESCRIPTIOM_LENGHT],
        ];
    }

    /**
     * 
     * @param User $user
     * @param Post $post
     */
    public function __construct($a = 'User $user',$b = 'Post $post') {
        $this->user = $a;
        $this->post = $b;
    }

    /**
     * @return boolean
     */
    public function save() {
        if ($this->validate()) {
            $comment = new Comment();
            $comment->text = $this->text;
            $comment->create_at = time();
            $comment->user_id = $this->user->getId();
            $comment->post_id = $this->post->getId();
            
            return $comment->save(false);
        }
    }

}
