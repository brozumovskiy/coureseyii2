<?php


namespace frontend\components;

use Yii;
use yii\base\Component;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

/**
 * Description of Storage
 *
 * @author bogda
 */

class Storage extends Component implements StorageInterface {
    
    private $fileName;
    
    /**
     * Save given UploadedFile instance to disk
     * @param UploadedFile $file
     * @return string|null
     */
    public function saveUploadedFile(UploadedFile $file) {
        $path = $this->preparePath($file);
        
        if ($path && $file->saveAs($path)){
            return $this->fileName;
        }
    }
    
    /*
     * Prepare path to save uploaded file
     * @param UploadedFile $file
     * @return string|null 
     */
    protected function preparePath(UploadedFile $file){
        $this->fileName = $this->getFileName($file);
        // d2/a4/213123g321321h421421m3213213m321b3bh21.jpg
        
        $path = $this->getStoragePath() . $this->fileName;
        // /var/www/project/frontend/web//uploads/d2/a4/213123g321321h421421m3213213m321b3bh21.jpg
        
        $path = FileHelper::normalizePath($path);
        if (FileHelper::createDirectory(dirname($path))){
            return $path;
        }
    }
    
    /**
     * @param UploadedFile $file
     * @return string
     */
    
    protected function getFileName(UploadedFile $file){
        //$file->tempname - /tmp/qwrwe
        
        $hash = sha1_file($file->tempName); //d2a4213123g321321h421421m3213213m321b3bh21
        
        $name = substr_replace($hash, '/', 2, 0);
        $name = substr_replace($name, '/', 5, 0); //d2/a4/213123g321321h421421m3213213m321b3bh21
        return $name . '.' . $file->extension; //d2/a4/213123g321321h421421m3213213m321b3bh21.jpg
    }
    
    /**
     * @return string
     */
    public function getStoragePath(){
        return Yii::getAlias(Yii::$app->params['storagePath']);
    }
    
    /**
     * 
     * @param string $filename
     * @return string
     */
    public function getFile(string $filename){
        return Yii::$app->params['storageUri'].$filename;
    }
    
    /**
     * @param string $filename
     * @return boolean
     */
    public function deleteFile(string $filename)
    {
        $file = $this->getStoragePath().$filename;
        
        if (file_exists($file)) {
            // Если файл существует, удаляем
            return unlink($file);
        }
        
        // Файла нет - хорошо. И удалять не нужно
        return true;
    }
}
